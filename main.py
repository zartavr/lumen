from funcs import *
from PIL import GifImagePlugin
from PIL import Image
from PIL import ImageSequence
import os

GifImagePlugin.LOADING_STRATEGY = GifImagePlugin.LoadingStrategy.RGB_AFTER_DIFFERENT_PALETTE_ONLY

img = Image.open("jaba.gif")

frame_size = img.size

print("Frames:", end=" ")
print(img.n_frames, end="\t")
print("animated:", end=" ")
print(img.is_animated, end="\t")
print()


pre_name = ""
act_name = ""
map_frame = Image.new("L", frame_size, "white")
for i in range(1, img.n_frames):
    img.seek(i)

    act_name = "frame" + str(i) + ".png"
    img.save("temporary/" + act_name, 'PNG')
    print("frame:", end=" ")
    print(i)

    if i == 1:
        pre_name = act_name
        continue

    pre_frame = Image.open("temporary/" + pre_name)
    act_frame = Image.open("temporary/" + act_name)

    for x in range(0, frame_size[0]):
        for y in range(0, frame_size[1]):

            p1 = pre_frame.getpixel((x, y))
            p2 = act_frame.getpixel((x, y))

            distance = calc_distance(p1, p2)

            k = 0
            if distance > 20:
                k = 0.3
            if distance > 50:
                k = 0.8

            px_mask = mark(map_frame.getpixel((x, y)), k)
            map_frame.putpixel((x, y), px_mask)

            px_filtered = do_mask(act_frame.getpixel((x, y)), px_mask)
            act_frame.putpixel((x, y), px_filtered)

    map_frame.save("temporary/mask_" + act_name)
    act_frame.save("temporary/" + act_name)

    pre_name = act_name

frames = []
duration_frame = []
maps = []
for i in range(1, img.n_frames):
    name = "temporary/frame" + str(i) + ".png"
    frame = Image.open(name)
    frames.append(frame)
    duration_frame.append(72)
duration_frame[-1] = 2000

frames[0].save(
    'result/jaba_filteged.gif',
    save_all=True,
    append_images=frames[1:],
    optimize=False,
    duration=duration_frame,
    loop=0
)


# with io.BytesIO() as output:
#     for i in range(0, img.n_frames):
#         img.seek(i)
#         img.save(output, "GIF")
#         frame = Image.open(output)
#         frame.save()

# print("frame:", end=" ")
# print(img.tell()+1)

# if i == 0:
#     pre_frame = output.getvalue()
#     continue
# frame = output.getvalue()
# print(pre_frame)
# print(frame)
