import math


def calc_distance(point1, point2):
    return math.sqrt((point2[0]-point1[0]) ** 2 +
                     (point2[1]-point1[1]) ** 2 +
                     (point2[2]-point1[2]) ** 2)


def mark(point, k):
    res = round(point - point * k)
    return res


def do_mask(px, px_mask):
    res = (round(px[0] * px_mask/255),
           round(px[1] * px_mask/255),
           round(px[2] * px_mask/255))
    return res
